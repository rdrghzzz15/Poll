from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.


class Profile(models.Model):
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    p_owner = models.OneToOneField(
        User, related_name='p_owned', on_delete=models.CASCADE)

    def __str__(self):
        return ('{} : {}'.format(self.first_name, self.last_name))


@receiver(post_save, sender=User)
def create_profile(sender, created, instance, **kwargs):
    if created:
        Profile.objects.create(p_owner=instance)
