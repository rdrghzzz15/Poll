from django.conf.urls import url
from .views import SignupView, SigninView, SignoutView, ProfileView

urlpatterns = [
	url(r'^profile/(?P<pk>\d+)/$', ProfileView.as_view(), name='profile'),
    url(r'^sign-up/$', SignupView.as_view(), name='sign-up'),
    url(r'^sign-in/$', SigninView.as_view(), name='sign-in'),
    url(r'^sign-out/$', SignoutView.as_view(), name='sign-out')
]