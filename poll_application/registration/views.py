from django.shortcuts import render, redirect
from django.views import View
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, login, logout
from .forms import RegistrationForm
from django.http import HttpResponse
from .models import Profile
from poll.models import Question

# Create your views here.

class SignupView(TemplateView):
    template_name = "registration/signup.html"

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home')
        context = {}
        context['form'] = RegistrationForm()
        return self.render_to_response(context)

    def post(self, request):
        form = RegistrationForm(self.request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        return self.render_to_response({'form': form, 'error': 'Error signing up.'})

class SigninView(TemplateView):
    template_name = 'poll_application/home.html'
    
    # def get(self,request, *args, **kwargs):
    #     context = {}
    #     if request.user.is_authenticated():
    #         return redirect('home')
    #     return self.render_to_response(context)

    def post(self, request):
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        
        if user:
            login(request,user)
            return redirect('home')
        
        context['error'] = 'Invalid Username or Password'
        context['questions'] = Question.objects.all().order_by('-when_created')
        return self.render_to_response(context)


class SignoutView(View):
    def get(self, request):
        logout(request)
        return redirect('home')

class ProfileView(TemplateView):
    template_name = 'registration/profile.html'

    # def get(self, request, pk, *args, **kwargs):
    #     context = self.get_context_data(pk)
    #     if not request.user.is_authenticated():
    #         return redirect('home')
    #     return self.render_to_response(context)

    # def get_context_data(self, pk, **kwargs):
    #     context = super(ProfileView, self).get_context_data(**kwargs)
    #     profile = get_object_or_404(User, pk=pk)
    #     context['profile'] = 
    #     return context