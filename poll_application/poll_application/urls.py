"""poll_application URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from .views import HomeView, QuestionView, AddQuestionView
from .views import AddChoiceView, VoteView, RemoveQuestion

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',HomeView.as_view(), name='home'),
    url(r'^registration/', include('registration.urls')),
    url(r'^add_question/$', AddQuestionView.as_view(), name='add_question'),
    url(r'^questions/(?P<pk>\d+)/$', QuestionView.as_view(), name='question'),
    url(r'^choices/$', AddChoiceView.as_view(), name='add_choice'),
    url(r'^vote/$', VoteView.as_view(), name='vote'),
    url(r'^remove_question/(?P<pk>\d+)/$', RemoveQuestion.as_view(), name='remove'),
]+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
