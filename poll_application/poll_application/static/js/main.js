require([
    'jquery',
    'mustache.min'
],function($,Mustache){
    $( document ).ready(function() {
        console.log( "ready!" );
        var forms = $('form.vote_form')
        for(var ctr=0; ctr<forms.length; ctr++){
            forms[ctr].className='vote_form'+(ctr+1);
        }
    });
    
    $('#add_question').on('submit', function(e){
        e.preventDefault();
        var q_template = "<a class='list-group-item' href='{{q_url}}' >{{question}}</a> ";
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data:{
                question: $('#question').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val() 
            },
            success:function(data){
                if(data.status=="failed"){
                    alert('Failed');
                    // $('#question').val('');
                }
                else{
                    $('#question').val('');
                    $('#question_set').prepend(Mustache.render(q_template,data));
                    $('#nothing').remove();
                }
            }
        });
    });

    $('#add_choice').on('submit',function(e){
        e.preventDefault();
        var c_template = "<li class='list-group-item'><input type='radio'"+" name='choice' value='{{choice_pk}}'/> "+
                            "<label> {{text}}</label></li>";
        console.log($('#question_pk').val());
        $.ajax({
            type: 'POST',
            url: $('#add_choice').attr('action'),
            data:{
                choice: $('#choice').val(),
                question_id: $('#question_pk').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val() 
            },
            success:function(data){
                if(data.status=='failed'){
                    alert('Failed')
                }
                else{
                    $('#choice').val('')
                    $('#choices').append(Mustache.render(c_template,data))
                }
            }
        });
    });

    $('.vote_form').on('submit',function(e){
        e.preventDefault();
        
        console.log($("input[name='choice']:checked").val());
        
        if ($("input[name='choice']:checked").val() == undefined){
            alert('Failed to vote');
        }
        else{
            console.log($('#question_pk').val());
            $.ajax({
                type:'POST',
                url: $(this).attr('action'),
                data:{
                    question_id: $('#question_pk').val(),
                    chosen_id: ($("input[name='choice']:checked").val()),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                },
                success:function(){
                    alert('Successfully voted!');
                    location.reload();
                }
            });    
        }
        
    });
});