from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.base import TemplateView
from django.http import HttpResponse, JsonResponse
from poll.models import Question, Choice, Vote
from django.views import View
from django.urls import reverse

class HomeView(TemplateView):
    template_name = 'poll_application/home.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['questions'] = Question.objects.all().order_by('-when_created')
        return context

class AddQuestionView(View):

    def post(self, request, *args, **kwargs):
        question_text = request.POST['question']
        print(question_text)
        if question_text == '':
            return JsonResponse({'status':'failed'})
        question = Question.objects.create(question=question_text,q_owner=request.user)
        return JsonResponse({'question':question.question,
            'status':'success',
            'q_url':reverse('question',args=(question.pk,))
            })

class RemoveQuestion(View):
    def get(self,request, pk):
        question = Question.objects.get(pk=pk)
        if request.user == question.q_owner:
            question.delete()
            print(question)
            return redirect('home')
        return HttpResponse()

class QuestionView(TemplateView):
    
    template_name = 'poll_application/question.html'

    def get(self, request, pk):
        if not request.user.is_authenticated():
            return redirect('home')
        context = self.get_context_data(pk)
        return self.render_to_response(context)

    def get_context_data(self, pk, **kwargs):
        context = super(QuestionView, self).get_context_data(**kwargs)
        this_question = get_object_or_404(Question, pk=pk)
        choices = Choice.objects.filter(question=this_question)
        vote = Vote.objects.filter(question=this_question, voter=self.request.user)
        if vote:
            context['status'] = 'alreadyvoted'
        else:
            context['status'] = 'notvoted'
        context['question'] = this_question
        return context

class   AddChoiceView(View):

    def post(self, request):
        question_id = request.POST['question_id']
        question = Question.objects.get(pk=question_id)
        choice_text = request.POST['choice']
        print(choice_text)
        if choice_text == '':
            return JsonResponse({'status':'failed'})

        choice = Choice.objects.create(choice_text=choice_text, c_owner=request.user, question=question)
        return JsonResponse({
            'status':'success',
            'text': choice.choice_text,
            'vote':choice.votes,
            'choice_pk':choice.pk
            })

class VoteView(View):

    def post(self, request):
        print(request.POST['question_id'])
        question_id = request.POST['question_id']
        chosen_id = request.POST['chosen_id']
        choice = Choice.objects.get(pk=chosen_id)
        question = Question.objects.get(pk=question_id)
        print(question)
        choice.votes += 1
        choice.save()
        Vote.objects.create(question=question,
            choice=choice, 
            voter=request.user
            )
        return HttpResponse()
        # return redirect(reverse('question',args=(question.pk,)))