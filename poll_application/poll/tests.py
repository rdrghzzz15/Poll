from django.test import TestCase
from .models import Question, Choice
from datetime import timedelta
from django.contrib.auth.models import User

# Create your tests here.

class PollModelTestCase(TestCase):
    """ Test for Poll model """

    def setUp(self):
        self.user1 = User.objects.create_user(username='User1', password='pass')
        self.user2 = User.objects.create_user(username='User2', password='pass')

    def test_question_create(self):
        """Test creating a poll question with attribute question and when_created"""
        self.assertEqual(Question.objects.count(), 0)
        self.assertEqual(self.user1.q_owned.count(), 0)
        self.assertEqual(self.user2.q_owned.count(), 0)

        question1 = Question.objects.create(question='Favorite Subject', q_owner=self.user1)
        question2 = Question.objects.create(question='Favorite Song', q_owner=self.user2)
    
    def test_choice_create(self):
        """Test creating poll choice"""
        question1 = Question.objects.create(question='Favorite Subject', q_owner=self.user1)
        question2 = Question.objects.create(question='Favorite Song', q_owner=self.user2)

        Choice.objects.create(question=question1, choice_text="CMSC105", c_owner=self.user1)
        Choice.objects.create(question=question1, choice_text="CMSC123", c_owner=self.user1)
        Choice.objects.create(question=question1, choice_text="CMSC137", c_owner=self.user1)
        

    def test_question_delete(self):
        """Test deleting poll question"""
        pass
    def test_choice_delete(self):
        """Test deleting a choice.""" 