# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-28 11:35
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('poll', '0003_auto_20170827_2344'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('choice', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='c_vote', to='poll.Choice')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='q_vote', to='poll.Question')),
                ('voter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='u_vote', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
