from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Question(models.Model):
    question = models.CharField(max_length=255)
    when_created = models.DateTimeField(auto_now_add=True)
    q_owner = models.ForeignKey(
        User, related_name='q_owned', on_delete=models.CASCADE)

    def __str__(self):
        return ('{} : {}'.format(self.pk, self.question))


class Choice(models.Model):
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name='q_choice')
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    c_owner = models.ForeignKey(
        User, related_name='c_owned', on_delete=models.CASCADE)

    def __str__(self):
        return ('{}:{}-{}'.format(
            self.question, self.choice_text, self.c_owner))


class Vote(models.Model):
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name='q_vote')
    choice = models.ForeignKey(
        Choice, on_delete=models.CASCADE, related_name='c_vote')
    voter = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='u_vote')

    def __str__(self):
        return ('{}:{}'.format(self.question, self.voter))
