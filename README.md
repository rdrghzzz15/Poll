The application is a poll system application. It allows users to add questions 
    and respond to the queston through voting.

Its features includes:
    add question
    add choices to a question
    vote 

To experience the full features of the app, you must login. If you have no account,
    you may register by going to the registration page of the app. 
    
    Add Question - in the home page.
    Add Choice - click on the question and it will redirect you to the question
        page. There, you can add choices. Anyone can add choices.
    Vote - you must also click on the question and vote on the choice that you 
        think is the answer of the question.